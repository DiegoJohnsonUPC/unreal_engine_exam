#pragma once
#define PI 3.141592653589793
#include<math.h>
namespace CppCLRWinformsProjekt {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	private:
		Graphics^ Graficador;
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//
			Graficador= this->CreateGraphics();
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::TextBox^ radio_textBox;
	private: System::Windows::Forms::TextBox^ x_textBox;
	private: System::Windows::Forms::TextBox^ puntos_textBox;



	private: System::Windows::Forms::TextBox^ y_textBox;

	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::RichTextBox^ listaPuntos;

	protected:

	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->radio_textBox = (gcnew System::Windows::Forms::TextBox());
			this->x_textBox = (gcnew System::Windows::Forms::TextBox());
			this->puntos_textBox = (gcnew System::Windows::Forms::TextBox());
			this->y_textBox = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->listaPuntos = (gcnew System::Windows::Forms::RichTextBox());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(26, 34);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(40, 15);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Radio";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(26, 71);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(76, 15);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Punto centro";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(26, 106);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(109, 15);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Numero de puntos";
			// 
			// radio_textBox
			// 
			this->radio_textBox->Location = System::Drawing::Point(144, 34);
			this->radio_textBox->Name = L"radio_textBox";
			this->radio_textBox->Size = System::Drawing::Size(100, 20);
			this->radio_textBox->TabIndex = 3;
			// 
			// x_textBox
			// 
			this->x_textBox->Location = System::Drawing::Point(144, 71);
			this->x_textBox->Name = L"x_textBox";
			this->x_textBox->Size = System::Drawing::Size(34, 20);
			this->x_textBox->TabIndex = 4;
			// 
			// puntos_textBox
			// 
			this->puntos_textBox->Location = System::Drawing::Point(144, 106);
			this->puntos_textBox->Name = L"puntos_textBox";
			this->puntos_textBox->Size = System::Drawing::Size(100, 20);
			this->puntos_textBox->TabIndex = 5;
			// 
			// y_textBox
			// 
			this->y_textBox->Location = System::Drawing::Point(184, 71);
			this->y_textBox->Name = L"y_textBox";
			this->y_textBox->Size = System::Drawing::Size(34, 20);
			this->y_textBox->TabIndex = 6;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(29, 146);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(106, 22);
			this->button1->TabIndex = 7;
			this->button1->Text = L"Graficar";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// listaPuntos
			// 
			this->listaPuntos->Location = System::Drawing::Point(330, 31);
			this->listaPuntos->Name = L"listaPuntos";
			this->listaPuntos->Size = System::Drawing::Size(150, 126);
			this->listaPuntos->TabIndex = 8;
			this->listaPuntos->Text = L"";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(524, 366);
			this->Controls->Add(this->listaPuntos);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->y_textBox);
			this->Controls->Add(this->puntos_textBox);
			this->Controls->Add(this->x_textBox);
			this->Controls->Add(this->radio_textBox);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"Form1";
			this->Text = L"Graficar Circunferencia";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
		
		int N = int::Parse(puntos_textBox->Text);
		float r = (float)(Convert::ToDouble(radio_textBox->Text));
		float px = (float)(Convert::ToDouble(x_textBox->Text));
		float py = (float)(Convert::ToDouble(y_textBox->Text));
		float x;
		float y;
		float angulo = 0;
		float incrementoAngulo = float((float)180 / (float)(N - 1));
		listaPuntos->Text += "Lista de puntos:\n";
		//linea 1
		Point pl1(px + 98, py + 202);
		Point pl2(px + 102, py + 198);
		Graficador->DrawLine(Pens::Red, pl1, pl2);
		// linea 2
		Point pl3(px + 98, py + 198);
		Point pl4(px + 102, py + 202);
		Graficador->DrawLine(Pens::Red, pl3, pl4);
		for (int i = 1; i <= N; i++) {
			float anguloRadianes = angulo * PI / 180;
			x = r * cos(anguloRadianes)+px;
			y = r * sin(anguloRadianes)+py;
			//linea 1
			Point p1(x+98, y+202);
			Point p2(x+102, y+198);
			Graficador->DrawLine(Pens::Red, p1, p2);
			// linea 2
			Point p3(x + 98, y + 198);
			Point p4(x + 102, y + 202);
			Graficador->DrawLine(Pens::Red, p3, p4);
			listaPuntos->Text += "(" + x + " , " + y + ")"+"\n";
			angulo += incrementoAngulo;
		}
	
		
	}
};
}
