#include<iostream>

using namespace std;

bool colision(float RA[4], float RB[4]) {
	float ld1 = RA[0] + (RA[2] / 2); //lado der del primer cuadrado
	float li1 = RA[0] - (RA[2] / 2);	 //lado izq del primer cuadrado	
	float ld2 = RB[0] + (RB[2] / 2); //lado der del segundo cuadrado
	float li2 = RB[0] - (RB[2] / 2); //lado izq del segundo cuadrado
	float la1 = RA[1] + (RA[3] / 2); //lado arr del primer cuadrado
	float lb1 = RA[1] - (RA[3] / 2); //lado aba del primer cuadrado
	float la2 = RB[1] + (RB[3] / 2); //lado arr del segundo cuadrado
	float lb2 = RB[1] - (RB[3] / 2); //lado aba del segundo cuadrado
	if (ld1 >= li2 && ld2 >= li1 && la1 >= lb2 && la2 >= lb2)return true;
	else return false;
}

int main() {
	//rectangulo A
	float xa;
	float ya;
	float wa;
	float ha;
	cout << "ingresa xa: ";
	cin >> xa;
	cout << "ingresa ya: ";
	cin >> ya;
	cout << "ingresa wa: ";
	cin >> wa;
	cout << "ingresa ha: ";
	cin >> ha;
	//rectangulo B
	float xb;
	float yb;
	float wb;
	float hb;
	cout << "ingresa xb: ";
	cin >> xb;
	cout << "ingresa yb: ";
	cin >> yb;
	cout << "ingresa wb: ";
	cin >> wb;
	cout << "ingresa hb: ";
	cin >> hb;
	//rectangulo C
	float xc;
	float yc;
	float wc;
	float hc;
	cout << "ingresa xc: ";
	cin >> xc;
	cout << "ingresa yc: ";
	cin >> yc;
	cout << "ingresa wc: ";
	cin >> wc;
	cout << "ingresa hc: ";
	cin >> hc;
	cout << endl;
	float RA[4] = { xa, ya, wa, ha };
	float RB[4] = { xb, yb, wb, hb };
	float RC[4] = { xc, yc, wc, hc };
	if (colision(RA, RB))cout << "colision entre el rectangulo A y el rectangulo B"<<endl;
	if (colision(RB, RC))cout << "colision entre el rectangulo B y el rectangulo C"<<endl;
	if (colision(RA, RC))cout << "colision entre el rectangulo A y el rectangulo C"<<endl;
	cin.get();
	cin.get();
	return 0;
}

