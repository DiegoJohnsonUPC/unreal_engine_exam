La mejor forma de solucionar este problema es implementar una 
pool de objetos. En lugar de eliminar los enemigos que el jugador 
destruye los puedes reutilizar reposicionandolos, cambiando sprites,
cambiando valores, etc. De esta manera reutilizas los espacios de 
memoria creados y optimizas el rendimiento del juego de muy buena 
manera.