#include "Orquesta.h"
#include <time.h>

Orquesta::Orquesta(int N, int R, int T) {
	this->N = N;
	this->R = R;
	this->T = T;
	types = { false,false };
	srand(time(NULL));
	CreateEnemies();
}
//GET
int Orquesta::GetN() {
	return N;
}
int Orquesta::GetR() {
	return R;
}
int Orquesta::GetT() {
	return T;
}
vector<bool> Orquesta::GetTypes() {
	return types;
}
vector<Enemy*> Orquesta::GetEnemies() {
	return activeEnemies;
}
//CREACION DE ENEMIGOS
void Orquesta::CreateEnemies() {
	//Creacion de enemigos
	//tipos:
	//0= terrestre, 1= aereo
	//rana
	vector<EnemyAttack*> rana_attacks;
	EnemyAttack* rana_ea;
	float rana_t = 5;
	vector<string> rana_v1 = { "flincher", "knockdown" };
	vector<string> rana_v2 = { "bullet" };
	rana_ea = new EnemyAttack(rana_t, rana_v1, rana_v2);
	rana_attacks.push_back(rana_ea);
	Enemy* rana = new Enemy(0, rana_attacks, "rana");
	activeEnemies.push_back(rana);
	//tronco
	vector<EnemyAttack*> tronco_attacks;
	EnemyAttack* tronco_ea;
	float tronco_t = 5;
	vector<string> tronco_v1 = { "charger", "distance" };
	vector<string> tronco_v2 = { "knockdown" };
	tronco_ea = new EnemyAttack(tronco_t, tronco_v1, tronco_v2);
	tronco_attacks.push_back(tronco_ea);
	Enemy* tronco = new Enemy(0, tronco_attacks, "tronco");
	activeEnemies.push_back(tronco);
	//mono
	vector<EnemyAttack*> mono_attacks;
	EnemyAttack* mono_ea;
	float mono_t = 5;
	vector<string> mono_v1 = { "distance", "bullet" };
	vector<string> mono_v2 = { "charger" };
	mono_ea = new EnemyAttack(mono_t, mono_v1, mono_v2);
	mono_attacks.push_back(mono_ea);
	Enemy* mono = new Enemy(1, mono_attacks, "mono");
	activeEnemies.push_back(mono);
}
//REALIZAR ATAQUE
void Orquesta::MakeAttack(int type) {
	cout << "El enemigo " << activeEnemies[type]->GetEnemyName() << " ha intentado atacar" << endl;
	if (types[activeEnemies[type]->GetType()] == false) {
		cout << "El sistema ha permitido el ataque de " << activeEnemies[type]->GetEnemyName() << endl;
		types[activeEnemies[type]->GetType()] = true;
		int r = rand() % 100 + 1;
		//cout << r;
		//probabilidad de ataque Orquesta
		if (r > R) {
			MakeOrquesta(type);
		}

	}
	else {
		cout << "El sistema no ha permitido que " << activeEnemies[type]->GetEnemyName() << " ataque porque alguien de su mismo tipo esta atacando" << endl;
	}
}

void Orquesta::MakeOrquesta(int type) {
	//cout << "orquestra";
	for (int i = 0; i < activeEnemies[type]->GetEnemyAttacks().size();i++) {
		for (int j = 0;j < activeEnemies.size();j++) {
			if (activeEnemies.at(j)->GetEnemyAttacks().at(0)->GetRT().at(0) == activeEnemies.at(type)->GetEnemyAttacks().at(0)->GetAT().at(0)) {
				cout << "El sistema ha originado un ataque orquesta entre " << activeEnemies[type]->GetEnemyName() << " y " << activeEnemies[j]->GetEnemyName() << endl;
			}
		}
	}
	
}
