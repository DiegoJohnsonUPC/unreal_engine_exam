#include "EnemyAttack.h"

EnemyAttack::EnemyAttack(float tiempoDuracion, vector<string> attackTags, vector<string> reactionTags) {
	this->tiempoDuracion = tiempoDuracion;
	this->attackTags = attackTags;
	this->reactionTags = reactionTags;
}
float EnemyAttack::GetTD() {
	return tiempoDuracion;
}

vector<string> EnemyAttack::GetAT() {
	return attackTags;
}

vector<string> EnemyAttack::GetRT() {
	return reactionTags;
}