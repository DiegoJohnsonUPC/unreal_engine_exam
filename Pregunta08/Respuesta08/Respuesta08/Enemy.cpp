#include "Enemy.h"

Enemy::Enemy(int t, vector<EnemyAttack*> a, string n){
	enemyType = t;
	enemyAttacks = a;
	enemyName = n;
}

int Enemy::GetType() {
	return enemyType;
}

string Enemy::GetEnemyName() {
	return enemyName;
}
vector<EnemyAttack*> Enemy::GetEnemyAttacks() {
	return enemyAttacks;
}
