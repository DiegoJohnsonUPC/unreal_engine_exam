#pragma once
#include"Enemy.h"
#include <iostream>

using namespace std;

class Orquesta
{
	//Variables
private:
	int N; //n�mero de ataques m�nimos necesarios luego de un ataque de orquesta para ejecutar otro ataque de orquestra
	int R; //variable de rango random que determina si un ataque de orquesta puede ser realizado o no
	int T; //tiempo m�nimo en el que un ataque de orquesta no puede ser activado luego de que el sistema ha activado otro ataque de orquestra

	int tempN;
	int tempT;

	//Orquestra
	vector<Enemy*> activeEnemies;
	vector<bool> types;
public:
	Orquesta(int N, int R, int T);
	int GetN();
	int GetR();
	int GetT();
	void CreateEnemies();
	vector<bool> GetTypes();
	vector<Enemy*> GetEnemies();
	void MakeAttack(int type);
	void MakeOrquesta(int type);
};

