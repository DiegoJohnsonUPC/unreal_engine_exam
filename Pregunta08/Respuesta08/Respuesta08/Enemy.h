#pragma once
#include "EnemyAttack.h"

using namespace std;

class Enemy
{
    
private:
    string enemyName;
    int enemyType;
    vector<EnemyAttack*> enemyAttacks;
public:
    Enemy(int t, vector<EnemyAttack*> a, string n);
    int GetType();
    string GetEnemyName();
    vector<EnemyAttack*> GetEnemyAttacks();


};

