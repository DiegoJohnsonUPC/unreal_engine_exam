#pragma once
#include<vector>
#include<string>

using namespace std;

class EnemyAttack
{   
private:
    float tiempoDuracion;
    vector<string> attackTags;
    vector<string> reactionTags;
public:
    EnemyAttack(float tiempoDuracion, vector<string> attackTags, vector<string> reactionTags);
    float GetTD();
    vector<string> GetAT();
    vector<string> GetRT();
};

