# Pseudocódigo de la implementación del Power Up Interlink

```c++
//distancia maxima para interlink
int X = 10;
//daño de interlink definido en 10
int interlinkDamage = 10

void OnMeleeAttackConnected(){
	//realizar daño al enemigo por el golpe melee
	enemy->DoDamage(MeleeAttackDamage); 
	//verificar si puede rebotar a otro enemigo
	if(InterlinkChance>Random(0,100)){
		//copiar la lista de enemigos
		activeEnemiesCopy = activeEnemies;
		//Eliminar al enemigo golpeado de la lista
		activeEnemiesCopy.Pop(this);
		bool repeatInterlink = true;
		//ciclo que repetira el interlink mientras haya probabilidad
		while(repeatInterlink && activeEnemiesCopy.size()>0){
			while(activeEnemiesCopy.size()>0){
				//se escoge un enemigo aleatoriamente
				int enemyHit = Random(0,activeEnemiesCopy.size()-1);
				//se verifica que la distancia sea menor a la establecida
				if(activeEnemiesCopy[enemyHit].DistanceWithPlayer()< X){
					//dibuja el efecto de interlink desde el enemigo hacia el proximo
					DrawInterlinkEffect(self, activeEnemiesCopy[enemyHit]);
					//daña al enemigo
					activeEnemiesCopy[enemyHit]->DoDamage(interlinkDamage);
					break;
				}
				else{
					//quita al enemigo de los posibles blancos de interlink
					activeEnemiesCopy.Pop(enemyHit);
				}
			}
			//posiblidad del 10% de repetirse el interlink
			if(11>Random(0,100)){
				break;
			}
		}	
	}
}
```
En caso de no contar con una lista de enemigos se puede intentar utilizar varios 
Raycast dentro del area para encontrar un posible enemigo cercano y marcarlo para 
posibles repeticiones.