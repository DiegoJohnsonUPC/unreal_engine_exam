#include "Enemies.h"

Enemies::Enemies(float p[3], bool iR, int id) {
	this->id = id;
	isRender = iR;
	for (int i = 0; i < 3;i++) {
		pos[i] = p[i];
	}
}
float* Enemies::GetPos() {
	return pos;
}
bool Enemies::GetIsRender() {
	return isRender;
}

int Enemies::GetId() {
	return id;
}
