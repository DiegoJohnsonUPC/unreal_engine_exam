#pragma once
class Enemies
{
private:
	bool isRender;
	float pos[3];
	int id;
public:
	Enemies(float p[3], bool iR,int id);
	float* GetPos();
	bool GetIsRender();
	int GetId();

};

