#include<iostream>
#include"Enemies.h"
#include <vector>

#define PI 3.141592653589793

using namespace std;


//Funcion para obtener el angulo entre dos vectores
float AngleBetweenTwoVectors(float p1[3], float p2[3]) {
	float num = (p1[0] * p2[0] + p1[1] * p2[1] + p1[2] * p2[2]);
	float den = sqrt(pow(p1[0], 2) + pow(p1[1], 2) + pow(p1[2], 2)) * sqrt(pow(p2[0], 2) + pow(p2[1], 2) + pow(p2[2], 2));
	float angle = acos(num/den) * 180 / PI;
	return angle;
}

float DistanceBetweenTwoPoints(float p1[3], float p2[3]) {
	float distance = sqrt(pow(p1[0]-p2[0], 2)+ pow(p1[1] - p2[1], 2)+ pow(p1[2] - p2[2], 2));
	return distance;
}

//Funcion para obtener el eje mas cercano a la direccion del jugador


int main() {
	float pos[3] = {0,0,0};//Posicion inicial del jugador
	float pForward[3] = { 0,1,0 };//Direccion a la que mira el jugador
	vector<Enemies*> activeEnemies; //Lista de enemigos
	//enemigos puestos como caso de prueba
	//1
	float epos1[3] = { 0,1,0 };
	activeEnemies.push_back(new Enemies(epos1, true, 1));
	//2
	float epos2[3] = { 8,2,7 };
	activeEnemies.push_back(new Enemies(epos2, true, 2));
	//3
	float epos3[3] = { 0,-1,0 };
	activeEnemies.push_back(new Enemies(epos3, true, 3));
	//4
	float epos4[3] = { 6,9,4};
	activeEnemies.push_back(new Enemies(epos4, true, 4));
	//5
	float epos5[3] = { 40,18,16 };
	activeEnemies.push_back(new Enemies(epos5, false, 5));

	//Variables necesarias para la simulacion
	int maxDistance = 20; //Distancia maxima a la que funciona el sistema de lock-on
	float visionDegrees = 60; //Grado de vision del jugador

	//Calculos
	//revisar a que eje esta mas proximo la vista del jugador

	Enemies* Target = NULL;
	bool front = false;//booleano que define si el enemigo esta en vision o no
	bool back = false;
	//booleanos que especifican si ya se encontro una prioridad
	bool pf = false; //ya se encontro un enemigo en vision?
	bool pe = false; //ya se encontro un enemigo en frente?

	float lastDistance = 80;
	//float radio = tan(60) * 20;
	for (int i = 0; i < activeEnemies.size();i++) {
		if (activeEnemies[i]->GetIsRender()) {//revisar si el enemigo esta siendo renderizado por la camara
			float distance = DistanceBetweenTwoPoints(pos, activeEnemies[i]->GetPos());
			if (distance < maxDistance) {//revisar si el enemigo esta a una distancia en la que el sistema Lock-on lo encuentre
				//revisar si el enemigo esta en el area de vision o detras
				float vDireccion[3] = { (pForward[0] - pos[0]),(pForward[1] - pos[1]),(pForward[2] - pos[2]) };
				float angle = AngleBetweenTwoVectors(vDireccion, activeEnemies[i]->GetPos()); //angulo entre los dos vectores
				if (angle <= visionDegrees)front = true;//revisar si esta en el area de vision
				if (angle >= 90)back = true;//revisar si esta atras del jugador
				if (Target == NULL) Target = activeEnemies[i]; //default
				if (front)//priorizar los que esten en vision
				{
					if (distance <= lastDistance) {//revisar el que tenga menos distancia al jugador
						cout << activeEnemies[i]->GetId() << endl;
						pf = true;
						Target = activeEnemies[i];
						lastDistance = distance;
					}
				}
				else if (!back) {//segundo los que no estan en vision pero estan frente al jugador
					if ((distance <= lastDistance) && !pf) {
						pe = true;
						Target = activeEnemies[i];
						lastDistance = distance;
					}

				}
				else {//por ultimo los que esten detras
					if ((distance <= lastDistance)&&!pf && !pe ){
						
						Target = activeEnemies[i];
						lastDistance = distance;
					}
				}
				

			}
			front = false;//reiniciar
			back = false;
		}
	}
	cout << "El target es el enemigo: " << Target->GetId();

	cin.get();
	cin.get();
	return 0;
}