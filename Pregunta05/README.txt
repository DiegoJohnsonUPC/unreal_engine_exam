Adjunto el proyecto con el codigo de mi solucion, cree 5 casos de prueba para encontrar 
al enemigo que se debe enfocar en el sistema y explico como plantee la solucion:
-Lo primero es definir funciones para conseguir el angulo entre vectores y la distancia entre 
puntos.
-Luego defino todas las entradas que piden en el ejercicio.
-Creo un posible Target como NULL para llenarlo con el enemigo mas indicado.
-Luego creo unos cuantos booleanos para definir las prioridades al elegir.
-Recorro la lista de enemigos y reviso que el enemigo este renderizado por la camara.
-Luego calculo la distancia del jugador al enemigo, si es mayor a 20 
como es definido en el caso no lo toma en cuenta como posible objetivo.
-Luego reviso si el enemigo esta en el campo de vision(definido como la mitad del cono completo)
o en caso contrario si esta al frente o detras.
-Para revisar si se encuentra en el angulo de vision, calculo el angulo formado entre el vector 
direccion(formado con la posicion y el punto al que mira) y el vector enemigo. Si el angulo formado
es mayor al angulo de vision predefinido esta fuera del campo de vision. Ademas, si el angulo es 
mator a 90 se encuentra detras del jugador.
-Gracias a esto puedo priorizar los enemigos en vision por sobre los que estan enfrente fuera de 
vision y los que estan por detras.
-El codigo se encuentra en Respuesta05.cpp y esta bastante comentado para que sea facil de entender
